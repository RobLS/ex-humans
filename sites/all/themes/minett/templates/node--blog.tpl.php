 <?php 
if (!$page) { 
?>
         <!-- Blog Item -->
              <div class="row">
                <div class="span1">
  
                  <div class="blog-icon">
                    <?php print render($content['field_icon']); ?>            
                  </div>
                     
                </div>
                <div class="span8">
                  <?php print render($content['field_custom']); ?>
          
                  <div class="row">
                    <div class="span8 post-d-info">
                      <h3><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
                      <div class="blue-dark">
                      <i class="icon-user"></i> By <?php print render($name) ?> <i class="icon-tag"></i>
                      <?php
// Query database table taxonomy_term_data and taxonomy_index
// So I can get all terms from my node.
$term = db_query('SELECT t.name, t.tid FROM {taxonomy_index} n LEFT JOIN  {taxonomy_term_data} t ON (n.tid = t.tid) WHERE n.nid = :nid', array(':nid' => $node->nid));

// db_query in Drupal 7 returns a stdClass object. 
// Value names are corresponding to the fields in your SQL query 
//(in our case "t.name") AND t.tid for build path.
$tags = '';
foreach ($term as $record) {
  // I put l() function for make my links.
  $tags .= l($record->name, 'taxonomy/term/' . $record->tid) . '  ';
}

print ' ' . $tags;
?> 
                       
                       
                       <i class="icon-comment-alt"></i> With  &nbsp; <?php if ($node->comment and !($node->comment == 1 and !$node->comment_count)) { ?>  <a class="comm" href="<?php print url("node/$node->nid", array('fragment' => 'comment-form')) ?>"><?php print format_plural($node->comment_count, '1 Comment', '@count Comments') ?></a><?php } ?>
                      </div>
                      <?php hide($content['comments']); hide($content['links']); print render($content); ?>

                    </div>
          
                  </div>
                </div>
              </div>
              <!-- Blog Item End -->
              
              <div class="row space40"></div>   
              
              <?php } else { 

$acc = user_load(1);
?>



            <!-- Blog Detail Content --> 
            <div class="row">
              <div class="span1">

              <div class="blog-icon">
                    <?php print render($content['field_icon']); ?>            
                  </div>
                     
                   
              </div>
              <div class="span8">

                <?php print render($content['field_blog_image']); ?>

  <div class="post-d-info">
                    <h3><?php print render($content['field_desc']); ?></h3>
                      <div class="blue-dark">
                      <i class="icon-user"></i> By <?php print render($name) ?> <i class="icon-tag"></i>
                      <?php
// Query database table taxonomy_term_data and taxonomy_index
// So I can get all terms from my node.
$term = db_query('SELECT t.name, t.tid FROM {taxonomy_index} n LEFT JOIN  {taxonomy_term_data} t ON (n.tid = t.tid) WHERE n.nid = :nid', array(':nid' => $node->nid));

// db_query in Drupal 7 returns a stdClass object. 
// Value names are corresponding to the fields in your SQL query 
//(in our case "t.name") AND t.tid for build path.
$tags = '';
foreach ($term as $record) {
  // I put l() function for make my links.
  $tags .= l($record->name, 'taxonomy/term/' . $record->tid) . '  ';
}

print ' ' . $tags;
?> 
                       
                       
<i class="icon-comment-alt"></i>   Width &nbsp; <?php if ($node->comment and !($node->comment == 1 and !$node->comment_count)) { ?>  <a class="comm" href="<?php print url("node/$node->nid", array('fragment' => 'comment-form')) ?>"><?php print format_plural($node->comment_count, '1 Comment', '@count Comments') ?></a><?php } ?>
                      </div>
                      <?php hide($content['comments']); hide($content['links']); print render($content); ?>
                      
                      <?php print render($content['comments']); ?>
                </div>
                </div>
                 </div>


<?php } ?>