
  <!-- Header -->
  <header id="header">
 
    <div class="container">
      <div class="row t-container">
        <div class="texture">
        <img src="<?php print $base_path . $directory; ?>/images/background.png"  alt="hi"  />
        </div>
        <!-- Logo -->
        <div class="span3">
         <?php if ($logo): ?>
       <div class="logo"><a href="<?php print $front_page; ?>"  rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a></div>
      <?php endif; ?>           
        </div>

    <div class="span9">
          <div class="row space90"></div>
          <nav class="navbar">
            <!-- Menu -->
<?php if ($page['main_menu']): ?>

<?php print render($page['main_menu']); ?>
<!--END: main_menu -->
<?php endif; ?>
          </nav>
          <div class="row space15 hidden-phone"></div>
        </div>

        <div class="span8">
          <div class="row">

<?php if ($page['maintop']): ?>
                 <div class="span6 offset2">
       <div id="maintop" class="sld">
          <?php print render($page['maintop']); ?>
       </div><!--END: maintop -->
         </div>  
      <?php endif; ?>
          
          </div>
        </div>
      </div>  
     

      
          <?php if ($page['slider']): ?>
           <div class="space50"></div>
          <div class="row">
        <div class="span12">  
          <!-- Slider -->
       <div id="slider" class="sld">
          <?php print render($page['slider']); ?>
       </div>
            <!-- Slider End -->
        </div>
      </div>
      <div class="row space30"></div>
      <?php endif; ?>
     
  </div> 
  </header>
  <!-- Header End -->

  <div class="row space50"></div>

  <!-- Content -->
  <div id="content">
    <div class="container">
      <div class="row">
      
            <?php
    $content_class = 'main';
 
      if ($page['sidebar_first']) {
        $content_class = 'span9';
      } else {
        $content_class = 'span12';
      }
   
    ?>
      
        <div class="<?php print $content_class; ?>">
         <div class="main">
          <?php print $messages; ?>

  <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
        </div>
        </div>
        <?php if ($page['sidebar_first']): ?>
        <!-- Side Bar -->  
          <div class="span3">
          <div class="space140"></div> 
      <?php print render($page['sidebar_first']); ?>
          </div>
      <?php endif; ?>
      
      
        
        <?php if($page['postscript_first'] || $page['postscript_second'] || $page['postscript_third'] || $page['postscript_fourth']) : ?>

      <div class="space40"></div>  
  
      <div class="postscript">
      
      
      <?php if ($page['postscript_first']): ?><div class="span8"><?php print render($page['postscript_first']); ?></div>
<?php endif; ?>
       
    
        <?php if ($page['postscript_second']): ?><div class="span4"><?php print render($page['postscript_second']); ?></div>
<?php endif; ?>
       
     
 <?php if ($page['postscript_third']): ?> <div class="span6"><?php print render($page['postscript_third']); ?></div>
<?php endif; ?>

   
     <?php if ($page['postscript_fourth']): ?> <div class="span6"><?php print render($page['postscript_fourth']); ?></div>
<?php endif; ?>

    </div>
    <!-- End postscript -->
      <?php endif; ?>
      
           <?php if($page['bottom_1'] || $page['bottom_2'] || $page['bottom_3'] || $page['bottom_4']) : ?>
    <div style="clear:both"></div><!-- Do not touch -->
     <div class="row2">
       
    
    <div id="bottom-wrapper" class="in<?php print (bool) $page['bottom_1'] + (bool) $page['bottom_2'] + (bool) $page['bottom_3'] + (bool) $page['bottom_4']; ?>">
          <?php if($page['bottom_1']) : ?>
          <div class="span3">
            <?php print render ($page['bottom_1']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_2']) : ?>
           <div class="span3">
            <?php print render ($page['bottom_2']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_3']) : ?>
           <div class="span3">
            <?php print render ($page['bottom_3']); ?>
          </div>
          <?php endif; ?>
          <?php if($page['bottom_4']) : ?>
          <div class="span3">
            <?php print render ($page['bottom_4']); ?>
          </div>
          <?php endif; ?>
      <div style="clear:both"></div>
     
      </div>
    </div><!-- end bottom -->
    <?php endif; ?>
              
  
     
 <?php if($page['footer_top']) : ?>
 <div class="space40"></div>  
  <div class="span12">  
          <div id="client-logo">
            <?php print render ($page['footer_top']); ?>
             <div class="clear"></div>
        </div>
         </div>
          <?php endif; ?>
           
      <div class="space50"></div> 
       </div>
    </div>
  </div>
  <!-- Content End -->
  
  <!-- Footer -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="span12 pos-r">
<?php if ($page['footer_first']): ?>

<?php print render($page['footer_first']); ?>

 <?php endif; ?>

          <div class="texture-footer">
           <img src="<?php print $base_path . $directory; ?>/images/background-footer.png"  alt="fi"  />
          </div>
        </div>
      </div>
        
      <div class="row">
        <div class="span5">
        <?php if ($page['footer_second']): ?>

<?php print render($page['footer_second']); ?>

 <?php endif; ?>

   <?php if ($page['footer_third']): ?>
<div class="row space10"></div>  
<?php print render($page['footer_third']); ?> 
<?php endif; ?>   
   </div>     
        <div class="span3 offset3">
             
          <div class="space80"></div>
          <?php if ($page['footer_fourth']): ?>

<?php print render($page['footer_fourth']); ?>

 <?php endif; ?>
 
          <div class="space70"></div> 
          
<?php if ($page['footer_fifth']): ?>

<?php print render($page['footer_fifth']); ?>

 <?php endif; ?>

        </div>
      </div>
      
<div class="row space30"></div>
      <div class="row">
        <div class="span6">
         
 
          <div class="logo-footer">
<a href="<?php print $front_page; ?>"  rel="home" id="logo2"><img src="<?php print $base_path . $directory; ?>/images/logo-footer.png"  alt="fi"  /></a>
               <?php if ($page['footer-a']): ?>
<?php print render($page['footer-a']); ?>
 <?php endif; ?>
          </div>                       
        </div>
        <div class="span6 right">
               
    <?php if ($page['footer-b']): ?>
<?php print render($page['footer-b']); ?>
 <?php endif; ?>

        </div>
      </div>

    </div>
  </footer>
  <!-- Footer End -->

 